﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR
{
    public partial class Form4 : Form
    {
        //Information de connexion

        MySqlConnection connexion = new MySqlConnection("database='gsb'; server='172.22.32.2'; uid='gsb'; Pwd='gsb';");
        int num_Pra;
        int id_cr = 1;
        string col_matricule;

        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {            
            try
            {
                //On voit si la connexion fonctionne
                connexion.Open();
                MySqlCommand cmd;
                MySqlDataReader reader;

                string sql = "SELECT * FROM `PRATICIEN` ";
                cmd = new MySqlCommand(sql, connexion);
                reader = cmd.ExecuteReader();

                while (reader.Read()) { cb_CR_Praticien.Items.Add(reader[1].ToString() + " " + reader[2].ToString()); }          
                reader.Close();

                try
                {
                    sql = "SELECT * FROM `COMPTE_RENDU`";
                    cmd = new MySqlCommand(sql, connexion);
                    reader = cmd.ExecuteReader();

                    reader.Read();
                    col_matricule = reader[0].ToString();
                    tb_CR_NumRapp.Text = reader[1].ToString();
                    num_Pra = Int32.Parse(reader[2].ToString());
                    tb_CR_DateRapp.Text = reader[3].ToString();
                    rtb_CR_Bilan.Text = reader[4].ToString();
                    tb_CR_Visite.Text = reader[5].ToString();
                    // Récupération des nouvelles données enregistrées dans la base de données
                    cb_CR_QualiteEcoute.Text = reader[6].ToString();
                    cb_CR_InteretPorte_Produit.Text = reader[7].ToString();
                    cb_CR_NiveauPrestation.Text = reader[8].ToString();
                    reader.Close();

                    sql = "SELECT * FROM `PRATICIEN` WHERE PRA_NUM =" + num_Pra;
                    cmd = new MySqlCommand(sql, connexion);
                    reader = cmd.ExecuteReader();

                    reader.Read();
                    cb_CR_Praticien.Text = reader[1].ToString() + " " + reader[2].ToString();
                    reader.Close();
                }
                catch
                {
                    MessageBox.Show("Erreur de cmd a la bdd");
                }  
            }
            catch (MySqlException co)
            {
                //En cas d'erreur, code d'erreur + Message perso
                MessageBox.Show(co.ToString());
                MessageBox.Show("Désolé la connexion n'a pas fonctionné");
            }
        }

        private void btn_CR_Details_Click(object sender, EventArgs e)
        {
            Form5 form5 = new Form5(cb_CR_Praticien.Text);
            form5.Show();
        }

        private void btn_CR_Fermer_Click(object sender, EventArgs e)
        {
            connexion.Close();
            this.Close();
        }

        private void btn_CR_Suivant_Click(object sender, EventArgs e)
        {
            id_cr++;
            string sqlS;
            sqlS = "SELECT * FROM `COMPTE_RENDU`";
            MySqlCommand cmdS = new MySqlCommand(sqlS, connexion);
            MySqlDataReader readerS = cmdS.ExecuteReader();
            string ancienNumRapp = tb_CR_NumRapp.Text;

            for (int i = 0; i < id_cr; i++)
            {
                readerS.Read();
            }

            tb_CR_NumRapp.Text = readerS[1].ToString();
            num_Pra = Int32.Parse(readerS[2].ToString());
            tb_CR_DateRapp.Text = readerS[3].ToString();
            rtb_CR_Bilan.Text = readerS[4].ToString();
            tb_CR_Visite.Text = readerS[5].ToString();
            // Récupération des nouvelles données enregistrées dans la base de données
            cb_CR_QualiteEcoute.Text = readerS[6].ToString();
            cb_CR_InteretPorte_Produit.Text = readerS[7].ToString();
            cb_CR_NiveauPrestation.Text = readerS[8].ToString();
            readerS.Close();

            sqlS = "SELECT * FROM `PRATICIEN` WHERE PRA_NUM =" + num_Pra;
            cmdS = new MySqlCommand(sqlS, connexion);
            readerS = cmdS.ExecuteReader();

            readerS.Read();
            cb_CR_Praticien.Text = readerS[1].ToString() + " " + readerS[2].ToString();
            readerS.Close();

            if (ancienNumRapp == tb_CR_NumRapp.Text) { id_cr--; }
        }

        private void btn_CR_Precedent_Click(object sender, EventArgs e)
        {
            if (id_cr != 1) { id_cr--; };
            string sqlS;
            string ancienNumRapp = tb_CR_NumRapp.Text;

            sqlS = "SELECT * FROM `COMPTE_RENDU`";
            MySqlCommand cmdS = new MySqlCommand(sqlS, connexion);
            MySqlDataReader readerS = cmdS.ExecuteReader();

            for (int i = 0; i < id_cr; i++)
            {
                readerS.Read();
            }
            
            tb_CR_NumRapp.Text = readerS[1].ToString();
            num_Pra = Int32.Parse(readerS[2].ToString());
            tb_CR_DateRapp.Text = readerS[3].ToString();
            rtb_CR_Bilan.Text = readerS[4].ToString();
            tb_CR_Visite.Text = readerS[5].ToString();
            // Récupération des nouvelles données enregistrées dans la base de données
            cb_CR_QualiteEcoute.Text = readerS[6].ToString();
            cb_CR_InteretPorte_Produit.Text = readerS[7].ToString();
            cb_CR_NiveauPrestation.Text = readerS[8].ToString();
            readerS.Close();

            sqlS = "SELECT * FROM `PRATICIEN` WHERE PRA_NUM =" + num_Pra;
            cmdS = new MySqlCommand(sqlS, connexion);
            readerS = cmdS.ExecuteReader();

            readerS.Read();
            cb_CR_Praticien.Text = readerS[1].ToString() + " " + readerS[2].ToString();
            readerS.Close();

            string sql = "SELECT * FROM `PRATICIEN` WHERE id =" + num_Pra.ToString();
            cmdS = new MySqlCommand(sqlS, connexion);
            readerS = cmdS.ExecuteReader();

            readerS.Read();
            cb_CR_Praticien.Items.Add(readerS[1].ToString() + " " + readerS[2].ToString());
            readerS.Close();
        }
        
        private void btn_CR_Nouveau_Click(object sender, EventArgs e)
        {
            Form6 form6 = new Form6();
            form6.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] pra_Pre_Nom = cb_CR_Praticien.Text.Split(' ');


            DateTime dateValue = DateTime.Parse(tb_CR_DateRapp.Text);
            string formatForMySql = dateValue.ToString("yyyy-MM-dd HH:mm:ss");
            string Date_rap = formatForMySql;

            string sqlS;

            sqlS = "SELECT COL_MATRICULE FROM `COMPTE_RENDU`";
            MySqlCommand cmdS = new MySqlCommand(sqlS, connexion);
            MySqlDataReader readerS = cmdS.ExecuteReader();

            readerS.Read().ToString();
            string col_mat = readerS[0].ToString();
            readerS.Close();

            sqlS = "SELECT PRA_NUM FROM `PRATICIEN` WHERE PRA_NOM = '" + pra_Pre_Nom[0] + "'";
            cmdS = new MySqlCommand(sqlS, connexion);
            readerS = cmdS.ExecuteReader();

            readerS.Read();
            string num_pra = readerS[0].ToString();    
            readerS.Close();

            sqlS = "UPDATE `COMPTE_RENDU` SET COL_MATRICULE = '" + col_mat + "'," +
                                            "RAP_NUM = '" + tb_CR_NumRapp.Text + "'," +
                                            "PRA_NUM = '" + num_pra + "'," +
                                            "RAP_DATE = '" + Date_rap + "'," +
                                            "RAP_BILAN = '" + rtb_CR_Bilan.Text + "'," +
                                            "RAP_MOTIF = '" + tb_CR_Visite.Text + "'," +
                                            // Modification des nouvelles données enregistrées dans la base de données
                                            "QUALITE_ECOUTE = '" + cb_CR_QualiteEcoute.Text + "'," +
                                            "INTERETPORTE_PRODUIT = '" + cb_CR_InteretPorte_Produit.Text + "'," +
                                            "NIVEAU_PRESCRIPTION = '" + cb_CR_NiveauPrestation.Text + "'" +
                                            "WHERE COL_MATRICULE = '" + col_mat + "' AND RAP_NUM = " + tb_CR_NumRapp.Text;

            cmdS = new MySqlCommand(sqlS, connexion);
            cmdS.ExecuteNonQuery();
        }
    }
}
