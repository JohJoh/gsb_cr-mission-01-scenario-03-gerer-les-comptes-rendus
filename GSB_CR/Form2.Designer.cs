﻿namespace GSB_CR
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.buttonCR = new System.Windows.Forms.Button();
            this.buttonVisiteur = new System.Windows.Forms.Button();
            this.buttonPraticien = new System.Windows.Forms.Button();
            this.buttonMedicament = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCR
            // 
            this.buttonCR.Image = ((System.Drawing.Image)(resources.GetObject("buttonCR.Image")));
            this.buttonCR.Location = new System.Drawing.Point(12, 12);
            this.buttonCR.Name = "buttonCR";
            this.buttonCR.Size = new System.Drawing.Size(75, 75);
            this.buttonCR.TabIndex = 0;
            this.buttonCR.UseVisualStyleBackColor = true;
            this.buttonCR.Click += new System.EventHandler(this.buttonCR_Click);
            // 
            // buttonVisiteur
            // 
            this.buttonVisiteur.Image = ((System.Drawing.Image)(resources.GetObject("buttonVisiteur.Image")));
            this.buttonVisiteur.Location = new System.Drawing.Point(93, 12);
            this.buttonVisiteur.Name = "buttonVisiteur";
            this.buttonVisiteur.Size = new System.Drawing.Size(75, 75);
            this.buttonVisiteur.TabIndex = 1;
            this.buttonVisiteur.UseVisualStyleBackColor = true;
            // 
            // buttonPraticien
            // 
            this.buttonPraticien.Image = ((System.Drawing.Image)(resources.GetObject("buttonPraticien.Image")));
            this.buttonPraticien.Location = new System.Drawing.Point(12, 93);
            this.buttonPraticien.Name = "buttonPraticien";
            this.buttonPraticien.Size = new System.Drawing.Size(75, 75);
            this.buttonPraticien.TabIndex = 2;
            this.buttonPraticien.UseVisualStyleBackColor = true;
            // 
            // buttonMedicament
            // 
            this.buttonMedicament.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMedicament.Image = ((System.Drawing.Image)(resources.GetObject("buttonMedicament.Image")));
            this.buttonMedicament.Location = new System.Drawing.Point(93, 93);
            this.buttonMedicament.Name = "buttonMedicament";
            this.buttonMedicament.Size = new System.Drawing.Size(75, 75);
            this.buttonMedicament.TabIndex = 3;
            this.buttonMedicament.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(180, 180);
            this.Controls.Add(this.buttonMedicament);
            this.Controls.Add(this.buttonPraticien);
            this.Controls.Add(this.buttonVisiteur);
            this.Controls.Add(this.buttonCR);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.Text = "Boîte à outils";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCR;
        private System.Windows.Forms.Button buttonVisiteur;
        private System.Windows.Forms.Button buttonPraticien;
        private System.Windows.Forms.Button buttonMedicament;
    }
}