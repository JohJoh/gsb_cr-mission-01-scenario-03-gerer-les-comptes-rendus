-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Jeu 23 Mai 2019 à 14:30
-- Version du serveur :  5.7.25-0ubuntu0.18.04.2
-- Version de PHP :  7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gsb`
--

-- --------------------------------------------------------

--
-- Structure de la table `CLIENT`
--

CREATE TABLE `CLIENT` (
  `CLI_ID` int(11) NOT NULL,
  `CLI_NOM` varchar(50) NOT NULL,
  `CLI_PRENOM` varchar(100) NOT NULL,
  `CLI_ADRESSE` varchar(100) NOT NULL,
  `CLI_CP` varchar(10) NOT NULL,
  `CLI_VILLE` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `COLLABORATEUR`
--

CREATE TABLE `COLLABORATEUR` (
  `COL_MATRICULE` varchar(20) NOT NULL,
  `COL_NOM` varchar(50) DEFAULT NULL,
  `COL_PRENOM` varchar(100) DEFAULT NULL,
  `COL_ADRESSE` varchar(100) DEFAULT NULL,
  `COL_CP` varchar(10) DEFAULT NULL,
  `COL_VILLE` varchar(60) DEFAULT NULL,
  `COL_DATE_EMBAUCHE` datetime DEFAULT NULL,
  `COL_LOGIN` varchar(25) DEFAULT NULL,
  `COL_PASS` varchar(25) DEFAULT NULL,
  `SEC_CODE` varchar(2) DEFAULT NULL,
  `LAB_CODE` varchar(4) NOT NULL,
  `idTYPE_COLLABORATEUR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `COLLABORATEUR`
--

INSERT INTO `COLLABORATEUR` (`COL_MATRICULE`, `COL_NOM`, `COL_PRENOM`, `COL_ADRESSE`, `COL_CP`, `COL_VILLE`, `COL_DATE_EMBAUCHE`, `COL_LOGIN`, `COL_PASS`, `SEC_CODE`, `LAB_CODE`, `idTYPE_COLLABORATEUR`) VALUES
('a131', 'Villechalane', 'Louis', '8 cours Lafontaine', '29000', 'BREST', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('a17', 'Andre', 'David', '1 r Aimon de Chissée', '38100', 'GRENOBLE', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('a55', 'Bedos', 'Christian', '1 r Bénédictins', '65000', 'TARBES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('a93', 'Tusseau', 'Louis', '22 r Renou', '86000', 'POITIERS', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('b13', 'Bentot', 'Pascal', '11 av 6 Juin', '67000', 'STRASBOURG', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('b16', 'Bioret', 'Luc', '1 r Linne', '35000', 'RENNES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('b19', 'Bunisset', 'Francis', '10 r Nicolas Chorier', '85000', 'LA ROCHE SUR YON', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('b25', 'Bunisset', 'Denise', '1 r Lionne', '49100', 'ANGERS', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('b28', 'Cacheux', 'Bernard', '114 r Authie', '34000', 'MONTPELLIER', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('b34', 'Cadic', 'Eric', '123 r Caponière', '41000', 'BLOIS', '2018-10-16 12:27:41', NULL, NULL, 'P', 'SW', 2),
('b4', 'Charoze', 'Catherine', '100 pl Géants', '33000', 'BORDEAUX', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 3),
('b50', 'Clepkens', 'Christophe', '12 r Fédérico Garcia Lorca', '13000', 'MARSEILLE', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 3),
('b59', 'Cottin', 'Vincenne', '36 sq Capucins', '5000', 'GAP', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('c14', 'Daburon', 'François', '13 r Champs Elysées', '6000', 'NICE', '2018-10-16 12:27:41', 'Daburon.François', 'daburon!2018', 'S', 'SW', 1),
('c3', 'De', 'Philippe', '13 r Charles Peguy', '10000', 'TROYES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('c54', 'Debelle', 'Michel', '181 r Caponière', '88000', 'EPINAL', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 3),
('d13', 'Debelle', 'Jeanne', '134 r Stalingrad', '44000', 'NANTES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('d51', 'Debroise', 'Michel', '2 av 6 Juin', '70000', 'VESOUL', '2018-10-16 12:27:41', NULL, NULL, 'E', 'GY', 3),
('e22', 'Desmarquest', 'Nathalie', '14 r Fédérico Garcia Lorca', '54000', 'NANCY', '2018-10-16 12:27:41', 'Desmarquest.Nathalie', 'desmarquest!2018', NULL, 'GY', 3),
('e24', 'Desnost', 'Pierre', '16 r Barral de Montferrat', '55000', 'VERDUN', '2018-10-16 12:27:41', NULL, NULL, 'E', 'SW', 1),
('e39', 'Dudouit', 'Frédéric', '18 quai Xavier Jouvin', '75000', 'PARIS', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('e49', 'Duncombe', 'Claude', '19 av Alsace Lorraine', '9000', 'FOIX', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 1),
('e5', 'Enault-Pascreau', 'Céline', '25B r Stalingrad', '40000', 'MONT DE MARSAN', '2018-10-16 12:27:41', NULL, NULL, 'S', 'GY', 1),
('e52', 'Eynde', 'Valérie', '3 r Henri Moissan', '76000', 'ROUEN', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 1),
('f21', 'Finck', 'Jacques', 'rte Montreuil Bellay', '74000', 'ANNECY', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('f39', 'Frémont', 'Fernande', '4 r Jean Giono', '69000', 'LYON', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('f4', 'Gest', 'Alain', '30 r Authie', '46000', 'FIGEAC', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('g19', 'Gheysen', 'Galassus', '32 bd Mar Foch', '75000', 'PARIS', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 3),
('g30', 'Girard', 'Yvon', '31 av 6 Juin', '80000', 'AMIENS', '2018-10-16 12:27:41', NULL, NULL, 'N', 'GY', 2),
('g53', 'Gombert', 'Luc', '32 r Emile Gueymard', '56000', 'VANNES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('g7', 'Guindon', 'Caroline', '40 r Mar Montgomery', '87000', 'LIMOGES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('h13', 'Guindon', 'François', '44 r Picotière', '19000', 'TULLE', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('h30', 'Igigabel', 'Guy', '33 gal Arlequin', '94000', 'CRETEIL', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 3),
('h35', 'Jourdren', 'Pierre', '34 av Jean Perrot', '15000', 'AURRILLAC', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('h40', 'Juttard', 'Pierre-Raoul', '34 cours Jean Jaurès', '8000', 'SEDAN', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 1),
('j45', 'Labouré-Morel', 'Saout', '38 cours Berriat', '52000', 'CHAUMONT', '2018-10-16 12:27:41', NULL, NULL, 'N', 'SW', 2),
('j50', 'Landré', 'Philippe', '4 av Gén Laperrine', '59000', 'LILLE', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('j8', 'Langeard', 'Hugues', '39 av Jean Perrot', '93000', 'BAGNOLET', '2018-10-16 12:27:41', NULL, NULL, 'P', 'GY', 1),
('k4', 'Lanne', 'Bernard', '4 r Bayeux', '30000', 'NIMES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('k53', 'Le', 'Noël', '4 av Beauvert', '68000', 'MULHOUSE', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('l14', 'Le', 'Jean', '39 r Raspail', '53000', 'LAVAL', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('l23', 'Leclercq', 'Servane', '11 r Quinconce', '18000', 'BOURGES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('l46', 'Lecornu', 'Jean-Bernard', '4 bd Mar Foch', '72000', 'LA FERTE BERNARD', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('l56', 'Lecornu', 'Ludovic', '4 r Abel Servien', '25000', 'BESANCON', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('m35', 'Lejard', 'Agnès', '4 r Anthoard', '82000', 'MONTAUBAN', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('m45', 'Lesaulnier', 'Pascal', '47 r Thiers', '57000', 'METZ', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('n42', 'Letessier', 'Stéphane', '5 chem Capuche', '27000', 'EVREUX', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('n58', 'Loirat', 'Didier', 'Les Pêchers cité Bourg la Croix', '45000', 'ORLEANS', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 3),
('n59', 'Maffezzoli', 'Thibaud', '5 r Chateaubriand', '2000', 'LAON', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('o26', 'Mancini', 'Anne', '5 r D\'Agier', '48000', 'MENDE', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 1),
('p32', 'Marcouiller', 'Gérard', '7 pl St Gilles', '91000', 'ISSY LES MOULINEAUX', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('p40', 'Michel', 'Jean-Claude', '5 r Gabriel Péri', '61000', 'FLERS', '2018-10-16 12:27:41', NULL, NULL, 'O', 'SW', 1),
('p41', 'Montecot', 'Françoise', '6 r Paul Valéry', '17000', 'SAINTES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 1),
('p42', 'Notini', 'Veronique', '5 r Lieut Chabal', '60000', 'BEAUVAIS', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('p49', 'Onfroy', 'Den', '5 r Sidonie Jacolin', '37000', 'TOURS', '2018-10-16 12:27:41', 'Onfroy.Den', 'onfroy!2018', NULL, 'GY', 2),
('p6', 'Pascreau', 'Charles', '57 bd Mar Foch', '64000', 'PAU', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('p7', 'Pernot', 'Claude-Noël', '6 r Alexandre 1 de Yougoslavie', '11000', 'NARBONNE', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('p8', 'Perrier', 'Maître', '6 r Aubert Dubayet', '71000', 'CHALON SUR SAONE', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('q17', 'Petit', 'Jean-Louis', '7 r Ernest Renan', '50000', 'SAINT LO', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 1),
('r24', 'Piquery', 'Patrick', '9 r Vaucelles', '14000', 'CAEN', '2018-10-16 12:27:41', NULL, NULL, 'O', 'GY', 2),
('r58', 'Quiquandon', 'Joël', '7 r Ernest Renan', '29000', 'QUIMPER', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('s10', 'Retailleau', 'Josselin', '88Bis r Saumuroise', '39000', 'DOLE', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 3),
('s21', 'Retailleau', 'Pascal', '32 bd Ayrault', '23000', 'MONTLUCON', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 2),
('t43', 'Souron', 'Maryse', '7B r Gay Lussac', '21000', 'DIJON', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('t47', 'Tiphagne', 'Patrick', '7B r Gay Lussac', '62000', 'ARRAS', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('t55', 'Tréhet', 'Alain', '7D chem Barral', '12000', 'RODEZ', '2018-10-16 12:27:41', NULL, NULL, NULL, 'SW', 1),
('t60', 'Tusseau', 'Josselin', '63 r Bon Repos', '28000', 'CHARTRES', '2018-10-16 12:27:41', NULL, NULL, NULL, 'GY', 2),
('zzz', 'swiss', 'bourdin', NULL, NULL, NULL, '2018-10-16 12:27:41', NULL, NULL, NULL, 'BC', 1);

-- --------------------------------------------------------

--
-- Structure de la table `COMPTE_RENDU`
--

CREATE TABLE `COMPTE_RENDU` (
  `COL_MATRICULE` varchar(20) NOT NULL,
  `RAP_NUM` int(11) NOT NULL DEFAULT '0',
  `PRA_NUM` int(11) NOT NULL,
  `RAP_DATE` datetime DEFAULT NULL,
  `RAP_BILAN` varchar(510) DEFAULT NULL,
  `RAP_MOTIF` varchar(510) DEFAULT NULL,
  `QUALITE_ECOUTE` int(1) DEFAULT NULL,
  `INTERETPORTE_PRODUIT` int(1) DEFAULT NULL,
  `NIVEAU_PRESCRIPTION` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `COMPTE_RENDU`
--

INSERT INTO `COMPTE_RENDU` (`COL_MATRICULE`, `RAP_NUM`, `PRA_NUM`, `RAP_DATE`, `RAP_BILAN`, `RAP_MOTIF`, `QUALITE_ECOUTE`, `INTERETPORTE_PRODUIT`, `NIVEAU_PRESCRIPTION`) VALUES
('a131', 54, 6, '2019-05-23 16:07:15', 'E4, modif', 'E4', 1, 2, 4),
('a17', 3, 23, '2018-10-03 10:32:32', 'Médecin curieux, à recontacer en décembre pour réunion', 'Actualisation annuelle', NULL, NULL, NULL),
('a17', 4, 4, '2018-10-03 10:32:32', 'Changement de direction, redéfinition de la politique médicamenteuse, recours au générique', 'Baisse activité', NULL, NULL, NULL),
('a17', 7, 41, '2018-10-03 10:32:32', 'RAS\nChangement de tel : 05 89 89 89 89', 'Rapport Annuel', NULL, NULL, NULL),
('b16', 54, 6, '2019-05-23 16:07:15', 'E4', 'E4', 1, 2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `FAMILLE`
--

CREATE TABLE `FAMILLE` (
  `FAM_CODE` varchar(6) NOT NULL,
  `FAM_LIBELLE` varchar(160) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `FAMILLE`
--

INSERT INTO `FAMILLE` (`FAM_CODE`, `FAM_LIBELLE`) VALUES
('AA', 'Antalgiques en association'),
('AAA', 'Antalgiques antipyrétiques en association'),
('AAC', 'Antidépresseur d\'action centrale'),
('AAH', 'Antivertigineux antihistaminique H1'),
('ABA', 'Antibiotique antituberculeux'),
('ABC', 'Antibiotique antiacnéique local'),
('ABP', 'Antibiotique de la famille des béta-lactamines (pénicilline A)'),
('AFC', 'Antibiotique de la famille des cyclines'),
('AFM', 'Antibiotique de la famille des macrolides'),
('AH', 'Antihistaminique H1 local'),
('AIM', 'Antidépresseur imipraminique (tricyclique)'),
('AIN', 'Antidépresseur inhibiteur sélectif de la recapture de la sérotonine'),
('ALO', 'Antibiotique local (ORL)'),
('ANS', 'Antidépresseur IMAO non sélectif'),
('AO', 'Antibiotique ophtalmique'),
('AP', 'Antipsychotique normothymique'),
('AUM', 'Antibiotique urinaire minute'),
('CRT', 'Corticoïde, antibiotique et antifongique à  usage local'),
('HYP', 'Hypnotique antihistaminique'),
('PSA', 'Psychostimulant, antiasthénique');

-- --------------------------------------------------------

--
-- Structure de la table `LABO`
--

CREATE TABLE `LABO` (
  `LAB_CODE` varchar(4) NOT NULL,
  `LAB_NOM` varchar(20) DEFAULT NULL,
  `LAB_CHEF_VENTE` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `LABO`
--

INSERT INTO `LABO` (`LAB_CODE`, `LAB_NOM`, `LAB_CHEF_VENTE`) VALUES
('BC', 'Bichat', 'Suzanne Terminus'),
('GY', 'Gyverny', 'Marcel MacDouglas'),
('SW', 'Swiss Kane', 'Alain Poutre');

-- --------------------------------------------------------

--
-- Structure de la table `MEDICAMENT`
--

CREATE TABLE `MEDICAMENT` (
  `MED_DEPOT_LEGAL` varchar(20) NOT NULL,
  `MED_NOM_COMMERCIAL` varchar(50) DEFAULT NULL,
  `FAM_CODE` varchar(6) NOT NULL,
  `MED_COMPOSITION` varchar(510) DEFAULT NULL,
  `MED_EFFETS` varchar(510) DEFAULT NULL,
  `MED_CONTRE_INDICATION` varchar(510) DEFAULT NULL,
  `MED_PRIX_ECHANTILLON` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `MEDICAMENT`
--

INSERT INTO `MEDICAMENT` (`MED_DEPOT_LEGAL`, `MED_NOM_COMMERCIAL`, `FAM_CODE`, `MED_COMPOSITION`, `MED_EFFETS`, `MED_CONTRE_INDICATION`, `MED_PRIX_ECHANTILLON`) VALUES
('3MYC7', 'TRIMYCINE', 'CRT', 'Triamcinolone (acétonide) + Néomycine + Nystatine', 'Ce médicament est un corticoïde à  activité forte ou très forte associé à  un antibiotique et un antifongique, utilisé en application locale dans certaines atteintes cutanées surinfectées.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants, d\'infections de la peau ou de parasitisme non traités, d\'acné. Ne pas appliquer sur une plaie, ni sous un pansement occlusif.', NULL),
('ADIMOL9', 'ADIMOL', 'ABP', 'Amoxicilline + Acide clavulanique', 'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux pénicillines ou aux céphalosporines.', NULL),
('AMOPIL7', 'AMOPIL', 'ABP', 'Amoxicilline', 'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux pénicillines. Il doit être administré avec prudence en cas d\'allergie aux céphalosporines.', NULL),
('AMOX45', 'AMOXAR', 'ABP', 'Amoxicilline', 'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.', 'La prise de ce médicament peut rendre positifs les tests de dépistage du dopage.', NULL),
('AMOXIG12', 'AMOXI Gé', 'ABP', 'Amoxicilline', 'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux pénicillines. Il doit être administré avec prudence en cas d\'allergie aux céphalosporines.', NULL),
('APATOUX22', 'APATOUX Vitamine C', 'ALO', 'Tyrothricine + Tétracaïne + Acide ascorbique (Vitamine C)', 'Ce médicament est utilisé pour traiter les affections de la bouche et de la gorge.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants, en cas de phénylcétonurie et chez l\'enfant de moins de 6 ans.', NULL),
('BACTIG10', 'BACTIGEL', 'ABC', 'Erythromycine', 'Ce médicament est utilisé en application locale pour traiter l\'acné et les infections cutanées bactériennes associées.', 'Ce médicament est contre-indiqué en cas d\'allergie aux antibiotiques de la famille des macrolides ou des lincosanides.', NULL),
('BACTIV13', 'BACTIVIL', 'AFM', 'Erythromycine', 'Ce médicament est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux macrolides (dont le chef de file est l\'érythromycine).', NULL),
('BITALV', 'BIVALIC', 'AAA', 'Dextropropoxyphène + Paracétamol', 'Ce médicament est utilisé pour traiter les douleurs d\'intensité modérée ou intense.', 'Ce médicament est contre-indiqué en cas d\'allergie aux médicaments de cette famille, d\'insuffisance hépatique ou d\'insuffisance rénale.', NULL),
('CARTION6', 'CARTION', 'AAA', 'Acide acétylsalicylique (aspirine) + Acide ascorbique (Vitamine C) + Paracétamol', 'Ce médicament est utilisé dans le traitement symptomatique de la douleur ou de la fièvre.', 'Ce médicament est contre-indiqué en cas de troubles de la coagulation (tendances aux hémorragies), d\'ulcère gastroduodénal, maladies graves du foie.', NULL),
('CLAZER6', 'CLAZER', 'AFM', 'Clarithromycine', 'Ce médicament est utilisé pour traiter des infections bactériennes spécifiques. Il est également utilisé dans le traitement de l\'ulcère gastro-duodénal, en association avec d\'autres médicaments.', 'Ce médicament est contre-indiqué en cas d\'allergie aux macrolides (dont le chef de file est l\'érythromycine).', NULL),
('DEPRIL9', 'DEPRAMIL', 'AIM', 'Clomipramine', 'Ce médicament est utilisé pour traiter les épisodes dépressifs sévères, certaines douleurs rebelles, les troubles obsessionnels compulsifs et certaines énurésies chez l\'enfant.', 'Ce médicament est contre-indiqué en cas de glaucome ou d\'adénome de la prostate, d\'infarctus récent, ou si vous avez reà§u un traitement par IMAO durant les 2 semaines précédentes ou en cas d\'allergie aux antidépresseurs imipraminiques.', NULL),
('DIMIRTAM6', 'DIMIRTAM', 'AAC', 'Mirtazapine', 'Ce médicament est utilisé pour traiter les épisodes dépressifs sévères.', 'La prise de ce produit est contre-indiquée en cas de d\'allergie à  l\'un des constituants.', NULL),
('DOLRIL7', 'DOLORIL', 'AAA', 'Acide acétylsalicylique (aspirine) + Acide ascorbique (Vitamine C) + Paracétamol', 'Ce médicament est utilisé dans le traitement symptomatique de la douleur ou de la fièvre.', 'Ce médicament est contre-indiqué en cas d\'allergie au paracétamol ou aux salicylates.', NULL),
('DORNOM8', 'NORMADOR', 'HYP', 'Doxylamine', 'Ce médicament est utilisé pour traiter l\'insomnie chez l\'adulte.', 'Ce médicament est contre-indiqué en cas de glaucome, de certains troubles urinaires (rétention urinaire) et chez l\'enfant de moins de 15 ans.', NULL),
('EQUILARX6', 'EQUILAR', 'AAH', 'Méclozine', 'Ce médicament est utilisé pour traiter les vertiges et pour prévenir le mal des transports.', 'Ce médicament ne doit pas être utilisé en cas d\'allergie au produit, en cas de glaucome ou de rétention urinaire.', NULL),
('EVILR7', 'EVEILLOR', 'PSA', 'Adrafinil', 'Ce médicament est utilisé pour traiter les troubles de la vigilance et certains symptomes neurologiques chez le sujet agé.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants.', NULL),
('INSXT5', 'INSECTIL', 'AH', 'Diphénydramine', 'Ce médicament est utilisé en application locale sur les piqûres d\'insecte et l\'urticaire.', 'Ce médicament est contre-indiqué en cas d\'allergie aux antihistaminiques.', NULL),
('JOVAI8', 'JOVENIL', 'AFM', 'Josamycine', 'Ce médicament est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux macrolides (dont le chef de file est l\'érythromycine).', NULL),
('LIDOXY23', 'LIDOXYTRACINE', 'AFC', 'Oxytétracycline +Lidocaïne', 'Ce médicament est utilisé en injection intramusculaire pour traiter certaines infections spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants. Il ne doit pas être associé aux rétinoïdes.', NULL),
('LITHOR12', 'LITHORINE', 'AP', 'Lithium', 'Ce médicament est indiqué dans la prévention des psychoses maniaco-dépressives ou pour traiter les états maniaques.', 'Ce médicament ne doit pas être utilisé si vous êtes allergique au lithium. Avant de prendre ce traitement, signalez à  votre médecin traitant si vous souffrez d\'insuffisance rénale, ou si vous avez un régime sans sel.', NULL),
('PARMOL16', 'PARMOCODEINE', 'AA', 'Codéine + Paracétamol', 'Ce médicament est utilisé pour le traitement des douleurs lorsque des antalgiques simples ne sont pas assez efficaces.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants, chez l\'enfant de moins de 15 Kg, en cas d\'insuffisance hépatique ou respiratoire, d\'asthme, de phénylcétonurie et chez la femme qui allaite.', NULL),
('PHYSOI8', 'PHYSICOR', 'PSA', 'Sulbutiamine', 'Ce médicament est utilisé pour traiter les baisses d\'activité physique ou psychique, souvent dans un contexte de dépression.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants.', NULL),
('PIRIZ8', 'PIRIZAN', 'ABA', 'Pyrazinamide', 'Ce médicament est utilisé, en association à  d\'autres antibiotiques, pour traiter la tuberculose.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants, d\'insuffisance rénale ou hépatique, d\'hyperuricémie ou de porphyrie.', NULL),
('POMDI20', 'POMADINE', 'AO', 'Bacitracine', 'Ce médicament est utilisé pour traiter les infections oculaires de la surface de l\'oeil.', 'Ce médicament est contre-indiqué en cas d\'allergie aux antibiotiques appliqués localement.', NULL),
('TROXT21', 'TROXADET', 'AIN', 'Paroxétine', 'Ce médicament est utilisé pour traiter la dépression et les troubles obsessionnels compulsifs. Il peut également être utilisé en prévention des crises de panique avec ou sans agoraphobie.', 'Ce médicament est contre-indiqué en cas d\'allergie au produit.', NULL),
('TXISOL22', 'TOUXISOL Vitamine C', 'ALO', 'Tyrothricine + Acide ascorbique (Vitamine C)', 'Ce médicament est utilisé pour traiter les affections de la bouche et de la gorge.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants et chez l\'enfant de moins de 6 ans.', NULL),
('URIEG6', 'URIREGUL', 'AUM', 'Fosfomycine trométamol', 'Ce médicament est utilisé pour traiter les infections urinaires simples chez la femme de moins de 65 ans.', 'La prise de ce médicament est contre-indiquée en cas d\'allergie à  l\'un des constituants et d\'insuffisance rénale.', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `OFFRIR`
--

CREATE TABLE `OFFRIR` (
  `VIS_MATRICULE` varchar(20) NOT NULL,
  `RAP_NUM` int(11) NOT NULL,
  `MED_DEPOT_LEGAL` varchar(20) NOT NULL,
  `OFF_QTE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `OFFRIR`
--

INSERT INTO `OFFRIR` (`VIS_MATRICULE`, `RAP_NUM`, `MED_DEPOT_LEGAL`, `OFF_QTE`) VALUES
('a17', 4, '3MYC7', 3),
('a17', 4, 'AMOX45', 12);

-- --------------------------------------------------------

--
-- Structure de la table `POSSEDER`
--

CREATE TABLE `POSSEDER` (
  `PRA_NUM` int(11) NOT NULL,
  `SPE_CODE` varchar(10) NOT NULL,
  `POS_DIPLOME` varchar(20) DEFAULT NULL,
  `POS_COEF_PRESCRIPTION` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `PRATICIEN`
--

CREATE TABLE `PRATICIEN` (
  `PRA_NUM` int(11) NOT NULL,
  `PRA_NOM` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PRA_PRENOM` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PRA_ADRESSE` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PRA_CP` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PRA_VILLE` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PRA_COEF_NOTORIETE` float DEFAULT NULL,
  `PRA_DATE_VISITE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TYP_CODE` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `PRATICIEN`
--

INSERT INTO `PRATICIEN` (`PRA_NUM`, `PRA_NOM`, `PRA_PRENOM`, `PRA_ADRESSE`, `PRA_CP`, `PRA_VILLE`, `PRA_COEF_NOTORIETE`, `PRA_DATE_VISITE`, `TYP_CODE`) VALUES
(1, 'Notini', 'Alain', '114 r Authie', '85000', 'LA ROCHE SUR YON', 290.03, '2018-10-16 14:11:51', 'MH'),
(2, 'Gosselin', 'Albert', '13 r Devon', '41000', 'BLOIS', 307.49, '2018-10-16 14:11:51', 'MV'),
(3, 'Delahaye', 'André', '36 av 6 Juin', '25000', 'BESANCON', 185.79, '2018-10-16 14:11:51', 'PS'),
(4, 'Leroux', 'André', '47 av Robert Schuman', '60000', 'BEAUVAIS', 172.04, '2018-10-16 14:11:51', 'PH'),
(5, 'Desmoulins', 'Anne', '31 r St Jean', '30000', 'NIMES', 94.75, '2018-10-16 14:11:51', 'PO'),
(6, 'Mouel', 'Anne', '27 r Auvergne', '80000', 'AMIENS', 45.2, '2018-10-16 14:11:51', 'MH'),
(7, 'Desgranges-Lentz', 'Antoine', '1 r Albert de Mun', '29000', 'MORLAIX', 20.07, '2018-10-16 14:11:51', 'MV'),
(8, 'Marcouiller', 'Arnaud', '31 r St Jean', '68000', 'MULHOUSE', 396.52, '2018-10-16 14:11:51', 'PS'),
(9, 'Dupuy', 'Benoit', '9 r Demolombe', '34000', 'MONTPELLIER', 395.66, '2018-10-16 14:11:51', 'PH'),
(10, 'Lerat', 'Bernard', '31 r St Jean', '59000', 'LILLE', 257.79, '2018-10-16 14:11:51', 'PO'),
(11, 'Marçais-Lefebvre', 'Bertrand', '86Bis r Basse', '67000', 'STRASBOURG', 450.96, '2018-10-16 14:11:51', 'MH'),
(12, 'Boscher', 'Bruno', '94 r Falaise', '10000', 'TROYES', 356.14, '2018-10-16 14:11:51', 'MV'),
(13, 'Morel', 'Catherine', '21 r Chateaubriand', '75000', 'PARIS', 379.57, '2018-10-16 14:11:51', 'PS'),
(14, 'Guivarch', 'Chantal', '4 av Gén Laperrine', '45000', 'ORLEANS', 114.56, '2018-10-16 14:11:51', 'PH'),
(15, 'Bessin-Grosdoit', 'Christophe', '92 r Falaise', '6000', 'NICE', 222.06, '2018-10-16 14:11:51', 'PO'),
(16, 'Rossa', 'Claire', '14 av Thiès', '6000', 'NICE', 529.78, '2018-10-16 14:11:51', 'MH'),
(17, 'Cauchy', 'Denis', '5 av Ste Thérèse', '11000', 'NARBONNE', 458.82, '2018-10-16 14:11:51', 'MV'),
(18, 'Gaffé', 'Dominique', '9 av 1ère Armée Française', '35000', 'RENNES', 213.4, '2018-10-16 14:11:51', 'PS'),
(19, 'Guenon', 'Dominique', '98 bd Mar Lyautey', '44000', 'NANTES', 175.89, '2018-10-16 14:11:51', 'PH'),
(20, 'Prévot', 'Dominique', '29 r Lucien Nelle', '87000', 'LIMOGES', 151.36, '2018-10-16 14:11:51', 'PO'),
(21, 'Houchard', 'Eliane', '9 r Demolombe', '49100', 'ANGERS', 436.96, '2018-10-16 14:11:51', 'MH'),
(22, 'Desmons', 'Elisabeth', '51 r Bernières', '29000', 'QUIMPER', 281.17, '2018-10-16 14:11:51', 'MV'),
(23, 'Flament', 'Elisabeth', '11 r Pasteur', '35000', 'RENNES', 315.6, '2018-10-16 14:11:51', 'PS'),
(24, 'Goussard', 'Emmanuel', '9 r Demolombe', '41000', 'BLOIS', 40.72, '2018-10-16 14:11:51', 'PH'),
(25, 'Desprez', 'Eric', '9 r Vaucelles', '33000', 'BORDEAUX', 406.85, '2018-10-16 14:11:51', 'PO'),
(26, 'Coste', 'Evelyne', '29 r Lucien Nelle', '19000', 'TULLE', 441.87, '2018-10-16 14:11:51', 'MH'),
(27, 'Lefebvre', 'Frédéric', '2 pl Wurzburg', '55000', 'VERDUN', 573.63, '2018-10-16 14:11:51', 'MV'),
(28, 'Lemée', 'Frédéric', '29 av 6 Juin', '56000', 'VANNES', 326.4, '2018-10-16 14:11:51', 'PS'),
(29, 'Martin', 'Frédéric', 'Bât A 90 r Bayeux', '70000', 'VESOUL', 506.06, '2018-10-16 14:11:51', 'PH'),
(30, 'Marie', 'Frédérique', '172 r Caponière', '70000', 'VESOUL', 313.31, '2018-10-16 14:11:51', 'PO'),
(31, 'Rosenstech', 'Geneviève', '27 r Auvergne', '75000', 'PARIS', 366.82, '2018-10-16 14:11:51', 'MH'),
(32, 'Pontavice', 'Ghislaine', '8 r Gaillon', '86000', 'POITIERS', 265.58, '2018-10-16 14:11:51', 'MV'),
(33, 'Leveneur-Mosquet', 'Guillaume', '47 av Robert Schuman', '64000', 'PAU', 184.97, '2018-10-16 14:11:51', 'PS'),
(34, 'Blanchais', 'Guy', '30 r Authie', '8000', 'SEDAN', 502.48, '2018-10-16 14:11:51', 'PH'),
(35, 'Leveneur', 'Hugues', '7 pl St Gilles', '62000', 'ARRAS', 7.39, '2018-10-16 14:11:51', 'PO'),
(36, 'Mosquet', 'Isabelle', '22 r Jules Verne', '76000', 'ROUEN', 77.1, '2018-10-16 14:11:51', 'MH'),
(37, 'Giraudon', 'Jean-Christophe', '1 r Albert de Mun', '38100', 'VIENNE', 92.62, '2018-10-16 14:11:51', 'MV'),
(38, 'Marie', 'Jean-Claude', '26 r Hérouville', '69000', 'LYON', 120.1, '2018-10-16 14:11:51', 'PS'),
(39, 'Maury', 'Jean-François', '5 r Pierre Girard', '71000', 'CHALON SUR SAONE', 13.73, '2018-10-16 14:11:51', 'PH'),
(40, 'Dennel', 'Jean-Louis', '7 pl St Gilles', '28000', 'CHARTRES', 550.69, '2018-10-16 14:11:51', 'PO'),
(41, 'Ain', 'Jean-Pierre', '4 résid Olympia', '2000', 'LAON', 5.59, '2018-10-16 14:11:51', 'MH'),
(42, 'Chemery', 'Jean-Pierre', '51 pl Ancienne Boucherie', '14000', 'CAEN', 396.58, '2018-10-16 14:11:51', 'MV'),
(43, 'Comoz', 'Jean-Pierre', '35 r Auguste Lechesne', '18000', 'BOURGES', 340.35, '2018-10-16 14:11:51', 'PS'),
(44, 'Desfaudais', 'Jean-Pierre', '7 pl St Gilles', '29000', 'BREST', 71.76, '2018-10-16 14:11:51', 'PH'),
(45, 'Phan', 'JérÃ´me', '9 r Clos Caillet', '79000', 'NIORT', 451.61, '2018-10-16 14:11:51', 'PO'),
(46, 'Riou', 'Line', '43 bd Gén Vanier', '77000', 'MARNE LA VALLEE', 193.25, '2018-10-16 14:11:51', 'MH'),
(47, 'Chubilleau', 'Louis', '46 r Eglise', '17000', 'SAINTES', 202.07, '2018-10-16 14:11:51', 'MV'),
(48, 'Lebrun', 'Lucette', '178 r Auge', '54000', 'NANCY', 410.41, '2018-10-16 14:11:51', 'PS'),
(49, 'Goessens', 'Marc', '6 av 6 Juin', '39000', 'DOLE', 548.57, '2018-10-16 14:11:51', 'PH'),
(50, 'Laforge', 'Marc', '5 résid Prairie', '50000', 'SAINT LO', 265.05, '2018-10-16 14:11:51', 'PO'),
(51, 'Millereau', 'Marc', '36 av 6 Juin', '72000', 'LA FERTE BERNARD', 430.42, '2018-10-16 14:11:51', 'MH'),
(52, 'Dauverne', 'Marie-Christine', '69 av Charlemagne', '21000', 'DIJON', 281.05, '2018-10-16 14:11:51', 'MV'),
(53, 'Vittorio', 'Myriam', '3 pl Champlain', '94000', 'BOISSY SAINT LEGER', 356.23, '2018-10-16 14:11:51', 'PS'),
(54, 'Lapasset', 'Nhieu', '31 av 6 Juin', '52000', 'CHAUMONT', 107, '2018-10-16 14:11:51', 'PH'),
(55, 'Plantet-Besnier', 'Nicole', '10 av 1ère Armée Française', '86000', 'CHATELLEREAULT', 369.94, '2018-10-16 14:11:51', 'PO'),
(56, 'Chubilleau', 'Pascal', '3 r Hastings', '15000', 'AURRILLAC', 290.75, '2018-10-16 14:11:51', 'MH'),
(57, 'Robert', 'Pascal', '31 r St Jean', '93000', 'BOBIGNY', 162.41, '2018-10-16 14:11:51', 'MV'),
(58, 'Jean', 'Pascale', '114 r Authie', '49100', 'SAUMUR', 375.52, '2018-10-16 14:11:51', 'PS'),
(59, 'Chanteloube', 'Patrice', '14 av Thiès', '13000', 'MARSEILLE', 478.01, '2018-10-16 14:11:51', 'PH'),
(60, 'Lecuirot', 'Patrice', 'résid St Pères 55 r Pigacière', '54000', 'NANCY', 239.66, '2018-10-16 14:11:51', 'PO'),
(61, 'Gandon', 'Patrick', '47 av Robert Schuman', '37000', 'TOURS', 599.06, '2018-10-16 14:11:51', 'MH'),
(62, 'Mirouf', 'Patrick', '22 r Puits Picard', '74000', 'ANNECY', 458.42, '2018-10-16 14:11:51', 'MV'),
(63, 'Boireaux', 'Philippe', '14 av Thiès', '10000', 'CHALON EN CHAMPAGNE', 454.48, '2018-10-16 14:11:51', 'PS'),
(64, 'Cendrier', 'Philippe', '7 pl St Gilles', '12000', 'RODEZ', 164.16, '2018-10-16 14:11:51', 'PH'),
(65, 'Duhamel', 'Philippe', '114 r Authie', '34000', 'MONTPELLIER', 98.62, '2018-10-16 14:11:51', 'PO'),
(66, 'Grigy', 'Philippe', '15 r Mélingue', '44000', 'CLISSON', 285.1, '2018-10-16 14:11:51', 'MH'),
(67, 'Linard', 'Philippe', '1 r Albert de Mun', '81000', 'ALBI', 486.3, '2018-10-16 14:11:51', 'MV'),
(68, 'Lozier', 'Philippe', '8 r Gaillon', '31000', 'TOULOUSE', 48.4, '2018-10-16 14:11:51', 'PS'),
(69, 'Dechâtre', 'Pierre', '63 av Thiès', '23000', 'MONTLUCON', 253.75, '2018-10-16 14:11:51', 'PH'),
(70, 'Goessens', 'Pierre', '22 r Jean Romain', '40000', 'MONT DE MARSAN', 426.19, '2018-10-16 14:11:51', 'PO'),
(71, 'Leménager', 'Pierre', '39 av 6 Juin', '57000', 'METZ', 118.7, '2018-10-16 14:11:51', 'MH'),
(72, 'Née', 'Pierre', '39 av 6 Juin', '82000', 'MONTAUBAN', 72.54, '2018-10-16 14:11:51', 'MV'),
(73, 'Guyot', 'Pierre-Laurent', '43 bd Gén Vanier', '48000', 'MENDE', 352.31, '2018-10-16 14:11:51', 'PS'),
(74, 'Chauchard', 'Roger', '9 r Vaucelles', '13000', 'MARSEILLE', 552.19, '2018-10-16 14:11:51', 'PH'),
(75, 'Mabire', 'Roland', '11 r Boutiques', '67000', 'STRASBOURG', 422.39, '2018-10-16 14:11:51', 'PO'),
(76, 'Leroy', 'Soazig', '45 r Boutiques', '61000', 'ALENCON', 570.67, '2018-10-16 14:11:51', 'MH'),
(77, 'Guyot', 'Stéphane', '26 r Hérouville', '46000', 'FIGEAC', 28.85, '2018-10-16 14:11:51', 'MV'),
(78, 'Delposen', 'Sylvain', '39 av 6 Juin', '27000', 'DREUX', 292.01, '2018-10-16 14:11:51', 'PS'),
(79, 'Rault', 'Sylvie', '15 bd Richemond', '2000', 'SOISSON', 526.6, '2018-10-16 14:11:51', 'PH'),
(80, 'Renouf', 'Sylvie', '98 bd Mar Lyautey', '88000', 'EPINAL', 425.24, '2018-10-16 14:11:51', 'PO'),
(81, 'Alliet-Grach', 'Thierry', '14 av Thiès', '7000', 'PRIVAS', 451.31, '2018-10-16 14:11:51', 'MH'),
(82, 'Bayard', 'Thierry', '92 r Falaise', '42000', 'SAINT ETIENNE', 271.71, '2018-10-16 14:11:51', 'MV'),
(83, 'Gauchet', 'Thierry', '7 r Desmoueux', '38100', 'GRENOBLE', 406.1, '2018-10-16 14:11:51', 'PS'),
(84, 'Bobichon', 'Tristan', '219 r Caponière', '9000', 'FOIX', 218.36, '2018-10-16 14:11:51', 'PH'),
(85, 'Duchemin-Laniel', 'Véronique', '130 r St Jean', '33000', 'LIBOURNE', 265.61, '2018-10-16 14:11:51', 'PO'),
(86, 'Laurent', 'Younès', '34 r Demolombe', '53000', 'MAYENNE', 496.1, '2018-10-16 14:11:51', 'MH');

-- --------------------------------------------------------

--
-- Structure de la table `PRODUCTION`
--

CREATE TABLE `PRODUCTION` (
  `PRO_ID` int(11) NOT NULL,
  `PRO_ECONOMIE` float NOT NULL,
  `PRO_RENTABILISATION` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `PRODUIT`
--

CREATE TABLE `PRODUIT` (
  `PRO_ID` int(11) NOT NULL,
  `PRO_NOM` varchar(50) NOT NULL,
  `PRO_CONDITIONNEMENT` varchar(100) NOT NULL,
  `PRO_PERIODICITE` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `REGION`
--

CREATE TABLE `REGION` (
  `REG_CODE` varchar(4) NOT NULL,
  `SEC_CODE` varchar(2) NOT NULL,
  `REG_NOM` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `REGION`
--

INSERT INTO `REGION` (`REG_CODE`, `SEC_CODE`, `REG_NOM`) VALUES
('AL', 'E', 'Alsace Lorraine'),
('AQ', 'S', 'Aquitaine'),
('AU', 'P', 'Auvergne'),
('BG', 'O', 'Bretagne'),
('BN', 'O', 'Basse Normandie'),
('BO', 'E', 'Bourgogne'),
('CA', 'N', 'Champagne Ardennes'),
('CE', 'P', 'Centre'),
('FC', 'E', 'Franche Comté'),
('HN', 'N', 'Haute Normandie'),
('IF', 'P', 'Ile de France'),
('LG', 'S', 'Languedoc'),
('LI', 'P', 'Limousin'),
('MP', 'S', 'Midi Pyrénée'),
('NP', 'N', 'Nord Pas de Calais'),
('PA', 'S', 'Provence Alpes Cote d\'Azur'),
('PC', 'O', 'Poitou Charente'),
('PI', 'N', 'Picardie'),
('PL', 'O', 'Pays de Loire'),
('RA', 'E', 'Rhone Alpes'),
('RO', 'S', 'Roussilon'),
('VD', 'O', 'Vendée');

-- --------------------------------------------------------

--
-- Structure de la table `SECTEUR`
--

CREATE TABLE `SECTEUR` (
  `SEC_CODE` varchar(2) NOT NULL,
  `SEC_LIBELLE` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `SECTEUR`
--

INSERT INTO `SECTEUR` (`SEC_CODE`, `SEC_LIBELLE`) VALUES
('E', 'Est'),
('N', 'Nord'),
('O', 'Ouest'),
('P', 'Paris centre'),
('S', 'Sud');

-- --------------------------------------------------------

--
-- Structure de la table `SPECIALITE`
--

CREATE TABLE `SPECIALITE` (
  `SPE_CODE` varchar(10) NOT NULL,
  `SPE_LIBELLE` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `SPECIALITE`
--

INSERT INTO `SPECIALITE` (`SPE_CODE`, `SPE_LIBELLE`) VALUES
('ACP', 'anatomie et cytologie pathologiques'),
('AMV', 'angéiologie, médecine vasculaire'),
('ARC', 'anesthésiologie et réanimation chirurgicale'),
('BM', 'biologie médicale'),
('CAC', 'cardiologie et affections cardio-vasculaires'),
('CCT', 'chirurgie cardio-vasculaire et thoracique'),
('CG', 'chirurgie générale'),
('CMF', 'chirurgie maxillo-faciale'),
('COM', 'cancérologie, oncologie médicale'),
('COT', 'chirurgie orthopédique et traumatologie'),
('CPR', 'chirurgie plastique reconstructrice et esthétique'),
('CU', 'chirurgie urologique'),
('CV', 'chirurgie vasculaire'),
('DN', 'diabétologie-nutrition, nutrition'),
('DV', 'dermatologie et vénéréologie'),
('EM', 'endocrinologie et métabolismes'),
('ETD', 'évaluation et traitement de la douleur'),
('GEH', 'gastro-entérologie et hépatologie (appareil digestif)'),
('GMO', 'gynécologie médicale, obstétrique'),
('GO', 'gynécologie-obstétrique'),
('HEM', 'maladies du sang (hématologie)'),
('MBS', 'médecine et biologie du sport'),
('MDT', 'médecine du travail'),
('MMO', 'médecine manuelle - ostéopathie'),
('MN', 'médecine nucléaire'),
('MPR', 'médecine physique et de réadaptation'),
('MTR', 'médecine tropicale, pathologie infectieuse et tropicale'),
('NEP', 'néphrologie'),
('NRC', 'neurochirurgie'),
('NRL', 'neurologie'),
('ODM', 'orthopédie dento maxillo-faciale'),
('OPH', 'ophtalmologie'),
('ORL', 'oto-rhino-laryngologie'),
('PEA', 'psychiatrie de l\'enfant et de l\'adolescent'),
('PME', 'pédiatrie maladies des enfants'),
('PNM', 'pneumologie'),
('PSC', 'psychiatrie'),
('RAD', 'radiologie (radiodiagnostic et imagerie médicale)'),
('RDT', 'radiothérapie (oncologie option radiothérapie)'),
('RGM', 'reproduction et gynécologie médicale'),
('RHU', 'rhumatologie'),
('STO', 'stomatologie'),
('SXL', 'sexologie'),
('TXA', 'toxicomanie et alcoologie');

-- --------------------------------------------------------

--
-- Structure de la table `TRAVAILLER`
--

CREATE TABLE `TRAVAILLER` (
  `VIS_MATRICULE` varchar(20) NOT NULL,
  `JJMMAA` datetime NOT NULL,
  `REG_CODE` varchar(4) NOT NULL,
  `TRA_ROLE` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `TYPE_COLLABORATEUR`
--

CREATE TABLE `TYPE_COLLABORATEUR` (
  `TYP_ID` int(11) NOT NULL,
  `TYP_LIBELLE` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `TYPE_COLLABORATEUR`
--

INSERT INTO `TYPE_COLLABORATEUR` (`TYP_ID`, `TYP_LIBELLE`) VALUES
(1, 'VISITEUR'),
(2, 'RESPONSABLE'),
(3, 'DELEGUE');

-- --------------------------------------------------------

--
-- Structure de la table `TYPE_PRATICIEN`
--

CREATE TABLE `TYPE_PRATICIEN` (
  `TYP_CODE` varchar(6) NOT NULL,
  `TYP_LIBELLE` varchar(50) DEFAULT NULL,
  `TYP_LIEU` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `TYPE_PRATICIEN`
--

INSERT INTO `TYPE_PRATICIEN` (`TYP_CODE`, `TYP_LIBELLE`, `TYP_LIEU`) VALUES
('MH', 'Médecin Hospitalier', 'Hopital ou clinique'),
('MV', 'Médecine de Ville', 'Cabinet'),
('PH', 'Pharmacien Hospitalier', 'Hopital ou clinique'),
('PO', 'Pharmacien Officine', 'Pharmacie'),
('PS', 'Personnel de santé', 'Centre paramédical');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `CLIENT`
--
ALTER TABLE `CLIENT`
  ADD PRIMARY KEY (`CLI_ID`);

--
-- Index pour la table `COLLABORATEUR`
--
ALTER TABLE `COLLABORATEUR`
  ADD PRIMARY KEY (`COL_MATRICULE`),
  ADD KEY `SEC_CODE` (`SEC_CODE`),
  ADD KEY `LAB_CODE` (`LAB_CODE`),
  ADD KEY `COLLABORATEURversTYPE_COLLABORATEUR` (`idTYPE_COLLABORATEUR`);

--
-- Index pour la table `COMPTE_RENDU`
--
ALTER TABLE `COMPTE_RENDU`
  ADD PRIMARY KEY (`COL_MATRICULE`,`RAP_NUM`),
  ADD KEY `PRA_NUM` (`PRA_NUM`),
  ADD KEY `VIS_MATRICULE_RAP_NUM` (`COL_MATRICULE`,`RAP_NUM`),
  ADD KEY `RAP_NUM` (`RAP_NUM`);

--
-- Index pour la table `FAMILLE`
--
ALTER TABLE `FAMILLE`
  ADD PRIMARY KEY (`FAM_CODE`);

--
-- Index pour la table `LABO`
--
ALTER TABLE `LABO`
  ADD PRIMARY KEY (`LAB_CODE`);

--
-- Index pour la table `MEDICAMENT`
--
ALTER TABLE `MEDICAMENT`
  ADD PRIMARY KEY (`MED_DEPOT_LEGAL`),
  ADD KEY `FAM_CODE` (`FAM_CODE`);

--
-- Index pour la table `OFFRIR`
--
ALTER TABLE `OFFRIR`
  ADD PRIMARY KEY (`VIS_MATRICULE`,`RAP_NUM`,`MED_DEPOT_LEGAL`),
  ADD KEY `MED_DEPOTLEGAL` (`MED_DEPOT_LEGAL`),
  ADD KEY `VIS_MATRICULE_RAP_NUM` (`VIS_MATRICULE`,`RAP_NUM`),
  ADD KEY `RAP_NUM` (`RAP_NUM`);

--
-- Index pour la table `POSSEDER`
--
ALTER TABLE `POSSEDER`
  ADD PRIMARY KEY (`PRA_NUM`,`SPE_CODE`),
  ADD KEY `SPE_CODE` (`SPE_CODE`);

--
-- Index pour la table `PRATICIEN`
--
ALTER TABLE `PRATICIEN`
  ADD PRIMARY KEY (`PRA_NUM`),
  ADD KEY `TYP_CODE` (`TYP_CODE`);

--
-- Index pour la table `PRODUCTION`
--
ALTER TABLE `PRODUCTION`
  ADD PRIMARY KEY (`PRO_ID`);

--
-- Index pour la table `PRODUIT`
--
ALTER TABLE `PRODUIT`
  ADD PRIMARY KEY (`PRO_ID`);

--
-- Index pour la table `REGION`
--
ALTER TABLE `REGION`
  ADD PRIMARY KEY (`REG_CODE`,`SEC_CODE`),
  ADD KEY `SEC_CODE` (`SEC_CODE`);

--
-- Index pour la table `SECTEUR`
--
ALTER TABLE `SECTEUR`
  ADD PRIMARY KEY (`SEC_CODE`);

--
-- Index pour la table `SPECIALITE`
--
ALTER TABLE `SPECIALITE`
  ADD PRIMARY KEY (`SPE_CODE`);

--
-- Index pour la table `TRAVAILLER`
--
ALTER TABLE `TRAVAILLER`
  ADD PRIMARY KEY (`VIS_MATRICULE`,`JJMMAA`,`REG_CODE`),
  ADD KEY `REG_CODE` (`REG_CODE`);

--
-- Index pour la table `TYPE_COLLABORATEUR`
--
ALTER TABLE `TYPE_COLLABORATEUR`
  ADD PRIMARY KEY (`TYP_ID`);

--
-- Index pour la table `TYPE_PRATICIEN`
--
ALTER TABLE `TYPE_PRATICIEN`
  ADD PRIMARY KEY (`TYP_CODE`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `CLIENT`
--
ALTER TABLE `CLIENT`
  MODIFY `CLI_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `PRODUCTION`
--
ALTER TABLE `PRODUCTION`
  MODIFY `PRO_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `PRODUIT`
--
ALTER TABLE `PRODUIT`
  MODIFY `PRO_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `TYPE_COLLABORATEUR`
--
ALTER TABLE `TYPE_COLLABORATEUR`
  MODIFY `TYP_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `COLLABORATEUR`
--
ALTER TABLE `COLLABORATEUR`
  ADD CONSTRAINT `COLLABORATEUR_ibfk_1` FOREIGN KEY (`LAB_CODE`) REFERENCES `LABO` (`LAB_CODE`),
  ADD CONSTRAINT `COLLABORATEUR_ibfk_2` FOREIGN KEY (`SEC_CODE`) REFERENCES `SECTEUR` (`SEC_CODE`),
  ADD CONSTRAINT `COLLABORATEURversTYPE_COLLABORATEUR` FOREIGN KEY (`idTYPE_COLLABORATEUR`) REFERENCES `TYPE_COLLABORATEUR` (`TYP_ID`);

--
-- Contraintes pour la table `COMPTE_RENDU`
--
ALTER TABLE `COMPTE_RENDU`
  ADD CONSTRAINT `COMPTE_RENDU_ibfk_1` FOREIGN KEY (`PRA_NUM`) REFERENCES `PRATICIEN` (`PRA_NUM`),
  ADD CONSTRAINT `COMPTE_RENDU_ibfk_2` FOREIGN KEY (`COL_MATRICULE`) REFERENCES `COLLABORATEUR` (`COL_MATRICULE`);

--
-- Contraintes pour la table `MEDICAMENT`
--
ALTER TABLE `MEDICAMENT`
  ADD CONSTRAINT `MEDICAMENT_ibfk_1` FOREIGN KEY (`FAM_CODE`) REFERENCES `FAMILLE` (`FAM_CODE`);

--
-- Contraintes pour la table `OFFRIR`
--
ALTER TABLE `OFFRIR`
  ADD CONSTRAINT `OFFRIR_ibfk_1` FOREIGN KEY (`MED_DEPOT_LEGAL`) REFERENCES `MEDICAMENT` (`MED_DEPOT_LEGAL`),
  ADD CONSTRAINT `OFFRIR_ibfk_2` FOREIGN KEY (`VIS_MATRICULE`,`RAP_NUM`) REFERENCES `COMPTE_RENDU` (`COL_MATRICULE`, `RAP_NUM`);

--
-- Contraintes pour la table `POSSEDER`
--
ALTER TABLE `POSSEDER`
  ADD CONSTRAINT `POSSEDER_ibfk_1` FOREIGN KEY (`PRA_NUM`) REFERENCES `PRATICIEN` (`PRA_NUM`),
  ADD CONSTRAINT `POSSEDER_ibfk_2` FOREIGN KEY (`SPE_CODE`) REFERENCES `SPECIALITE` (`SPE_CODE`);

--
-- Contraintes pour la table `PRATICIEN`
--
ALTER TABLE `PRATICIEN`
  ADD CONSTRAINT `PRATICIEN_ibfk_1` FOREIGN KEY (`TYP_CODE`) REFERENCES `TYPE_PRATICIEN` (`TYP_CODE`);

--
-- Contraintes pour la table `REGION`
--
ALTER TABLE `REGION`
  ADD CONSTRAINT `REGION_ibfk_1` FOREIGN KEY (`SEC_CODE`) REFERENCES `SECTEUR` (`SEC_CODE`);

--
-- Contraintes pour la table `TRAVAILLER`
--
ALTER TABLE `TRAVAILLER`
  ADD CONSTRAINT `TRAVAILLER_ibfk_1` FOREIGN KEY (`VIS_MATRICULE`) REFERENCES `COLLABORATEUR` (`COL_MATRICULE`),
  ADD CONSTRAINT `TRAVAILLER_ibfk_2` FOREIGN KEY (`REG_CODE`) REFERENCES `REGION` (`REG_CODE`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
